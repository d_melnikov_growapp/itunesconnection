//
//  ItunesConnection.swift
//  ItunesConnection
//
//  Created by Вячеслав Юрьевич Мельников on 20.02.2019.
//  Copyright © 2019 Денис Мельников. All rights reserved.
//

import UIKit

class ItunesConnection: NSObject {
    class func getAlbumForString(searchString: String, completionHandler: @escaping (Album)->()) {
        
        let escapedString = searchString.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlHostAllowed)
        
        let url = URL(string: "https://itunes.apple.com/search?term=\(escapedString!)&media=podcast")
        
        let task = URLSession.shared.dataTask(with: url!) { (data: Data!, response: URLResponse!, error: Error!) in
            if error == nil {
                let itunesDict = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as! NSDictionary
                print(itunesDict)
                
                let album = Album(collectionName: "Frozen", artistName: "Idina Menzel", artworkURL: "")
                completionHandler(album)
            }
        }
        task.resume()
    }
}
