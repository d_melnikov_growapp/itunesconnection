//
//  ViewController.swift
//  ItunesConnection
//
//  Created by Вячеслав Юрьевич Мельников on 20.02.2019.
//  Copyright © 2019 Денис Мельников. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        ItunesConnection.getAlbumForString(searchString: "Frozen") { (album: Album) in
            print(album)
        }
    }


}

