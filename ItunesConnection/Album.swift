//
//  Album.swift
//  ItunesConnection
//
//  Created by Вячеслав Юрьевич Мельников on 20.02.2019.
//  Copyright © 2019 Денис Мельников. All rights reserved.
//

import UIKit

class Album: NSObject {
    var collectionName: String
    var artistName: String
    var artworkUrl: String
    
    init(collectionName: String, artistName: String, artworkURL: String) {
        self.collectionName = collectionName
        self.artistName = artistName
        self.artworkUrl = artworkURL
    }
}
